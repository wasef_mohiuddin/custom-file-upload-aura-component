import { LightningElement, track, api } from 'lwc';
 
export default class FileUpload extends LightningElement {
 
 
    @api recordId;
    @track showModal = false;
    @track showUploadButton = false;
    @track othervalue;
 
    @track showTextField = false;
    @track otherFiletypeValue;
    @track showSpinner = false;
    @track type;
 
    connectedCallback() {
        console.log('recordId-id=>', this.recordId);
        
    }
 
    get typeOptions() {
        return [{
                label: "Document",
                value: "Document"
            },
            {
                label: "Other",
                value: "Other"
            },
            
        ];
    } 
 
    openModal(event){
        this.showModal = true;
    }
    
    closeModal() {
        this.clearAllFields();
        this.showModal = false;
    }
 
 
    handleInput(event) {
        this.type = event.target.value;
        this.othervalue = this.type;
        if (this.type) {
            if (this.type != 'Other') {
                this.showTextField = false;
                this.showUploadButton = true;
            } else {
                this.showTextField = true;
                this.othervalue = 'Other - ';
                this.showUploadButton = false;
                if(this.otherFiletypeValue != '' && this.otherFiletypeValue != null){
                    this.showUploadButton = true;
                }
            }
        }
    }
 
    handleTextInput(event) {
        this.othervalue = this.othervalue + event.target.value;
        this.otherFiletypeValue = event.target.value;
        // this.type = event.target.value;
        if (event.target.value != '' && event.target.value != null) {
            this.showUploadButton = true;
        } else {
            this.showUploadButton = false;
        }
 
    }
 
    handleUploadFinished(event) {
        this.showSpinner = true;
        const uploadedFiles = event.detail.files;
        let docId = uploadedFiles[0].documentId;
        console.log('docId: ', JSON.stringify(uploadedFiles) + docId);
            console.log('othervalue: ', this.othervalue);
        updateDocument({
            docId: docId,
            type: this.othervalue
        }).then(result => {
            this.clearAllFields();
        }).catch(error => {
            console.log('error: ', JSON.stringify(error));
            this.showUploadButton = false;
           // this.type = null;
        })
    }
 
    clearAllFields() {
        this.showUploadButton = false;
        this.type = null;
        this.showTextField = false;
        this.othervalue = null;
        this.otherFiletypeValue = null;
    }
 
}
